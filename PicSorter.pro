TEMPLATE = app
#DEFINES += 
TARGET = PicSorter
QT += core \
    gui 
HEADERS += include/main.h \ 
	include/Log.h \
	include/SettingsManager.h \
	include/common.h \
	include/MainWindow.h \
	include/PerfTimeCounter.h \
	include/cFileSystem.h \
	include/TreeviewDeligate.h \
	include/OperationMap.h \
	include/HotKeyFilter.h
	
INCLUDEPATH += include
SOURCES += src/main.cpp \
    src/Log.cpp \
	src/SettingsManager.cpp \
	src/MainWindow.cpp \
	src/PerfTimeCounter.cpp \
	src/cFileSystem.cpp \
	src/TreeviewDeligate.cpp \
	src/OperationMap.cpp \
	src/HotKeyFilter.cpp
	
RESOURCES = res/res.qrc
    
REVISION_INPUT = res/Revision_template.cpp
revtarget.input = REVISION_INPUT
revtarget.output = include/Revision.h
revtarget.commands = sh res/make_revision.sh ${QMAKE_FILE_NAME} ${QMAKE_FILE_OUT}
revtarget.depends = ${QMAKE_FILE_NAME} $$SOURCES $$HEADERS 
revtarget.name = Generating_RevNum (${QMAKE_FILE_NAME})
revtarget.variable_out = HEADERS
QMAKE_EXTRA_COMPILERS += revtarget
    
unix {
	CONFIG += debug_and_release
#	LIBS += -lmodbus \
#    	-L/usr/local/lib
    CONFIG(release, debug|release) {
    	DESTDIR = release  
    } 
    CONFIG(debug, debug|release) {
    	DEFINES += _GLIBCXX_DEBUG
   		DESTDIR = debug
   		CONFIG += console
   		QMAKE_CXXFLAGS_DEBUG += -pg
		QMAKE_CFLAGS_DEBUG += -pg
		QMAKE_LFLAGS_DEBUG += -pg
   	}
   	OBJECTS_DIR = $$(DESTDIR)
   	MOC_DIR = $$(DESTDIR)
}
win32 { 
#    LIBS += /usr/local/lib/libmodbus.a \
#        -L.
    INCLUDEPATH += /usr/local/include \
        c:/MinGW/include \
        d:/MinGW/include
    CONFIG(debug, debug|release) {
   		CONFIG += console
   		QMAKE_CXXFLAGS_DEBUG += -pg
		QMAKE_CFLAGS_DEBUG += -pg
		QMAKE_LFLAGS_DEBUG += -pg
   	}
}
