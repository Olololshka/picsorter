/*
 * Log.h
 *
 *  Created on: 29.06.2012
 *      Author: tolyan
 */

#ifndef LOG_H_
#define LOG_H_

#include <QObject>
#include <QString>
#include <QVariant>

#include "SettingsManager.h"

#define LOG_DEFAULT_FILENAME	"res/Log.%1.log"
#define LOG_DEFAULT_DATE_FORMAT "dd.MM.yyyy"
#define LOG_DEFAULT_TIME_FORMAT	"h:mm:ss.z"

#define LOG_FATAL(msg)		LOG->addMessage(msg, Log::LOG_FATAL, __FUNCTION__)
#define LOG_ERROR(msg)		LOG->addMessage(msg, Log::LOG_ERROR, __FUNCTION__)
#define LOG_WARNING(msg)	LOG->addMessage(msg, Log::LOG_WARNING, __FUNCTION__)
#define LOG_INFO(msg)		LOG->addMessage(msg, Log::LOG_INFO, __FUNCTION__)
#define LOG_DEBUG(msg)		LOG->addMessage(msg, Log::LOG_DEBUG, __FUNCTION__)
#define LOG_type(msg, type)	LOG->addMessage(msg, type, __FUNCTION__)

class QFile;
class QTextStream;
class QMutex;
class QLabel;

class Log: public QObject
    {
Q_OBJECT
public:
    enum enLogLevel
	{
	LOG_FATAL = 0,
	LOG_ERROR = 1,
	LOG_WARNING = 2,
	LOG_INFO = 3,
	LOG_DEBUG = 4,
	};

    Log(QObject* parent = NULL);
    ~Log();

    void setLogFileName(const QString& name);
    void setLogDateTimeFormat(
	    const QString& DateFormat = settings->value(
		    trUtf8("Log/LogDateFormat"),
		    trUtf8(LOG_DEFAULT_DATE_FORMAT)).toString(),
	    const QString& TimeFormat = settings->value(
		    trUtf8("Log/LogTimeFormat"),
		    trUtf8(LOG_DEFAULT_TIME_FORMAT)).toString());
    void setInfoString(QLabel* pInfoString = NULL);
    enum enLogLevel getLogLevel(void) const;

public slots:
    Q_INVOKABLE void addMessage(const QString& text, enLogLevel Level = LOG_INFO,
	    const char* additionalText = NULL);
    Q_INVOKABLE void setLogLevel(
	    enLogLevel = (enum enLogLevel) settings->value(trUtf8("Log/Level"),
		    LOG_INFO).toUInt());

private:
    QString LogDateFormat;
    QString LogTimeFormat;

    enLogLevel CurrentLogLevel;

    QMutex* OperationInProgressMutex;

    QFile* Logfile;
    QTextStream* textstream;

    QLabel* pInfoString;
    };

extern Log* LOG;

#endif /* LOG_H_ */
