/*
 * MainWindow.h
 *
 *  Created on: 22.08.2012
 *      Author: tolyan
 */

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>
#include <QMap>

#include "OperationMap.h"

class QTreeView;
class QFileSystemModel;
class QSplitter;
class QLabel;
class QLineEdit;
class QString;
class QItemSelectionModel;
class QItemSelection;
class cFileSystem;
class HotKeyFilter;
class QPushButton;
class QProgressBar;

class MainWindow: public QMainWindow
    {
Q_OBJECT
public:
    MainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);

private:
    QTreeView *filesList;
    cFileSystem *filesystem;

    QLabel *pic;
    QLineEdit *filterEdit;
    QItemSelectionModel* selectionModel;
    HotKeyFilter* hotkeyFilter;
    OperationMap* opMap;
    QMap<QChar, OperationMap::sOperation> opTemplate;
    bool fSelectionChanging;
    QPushButton* startButton;
    QProgressBar* progressBar;

    void ReadSettings(void);
    QString createFolder(const QString& ActionName);

public slots:
    void hotKeyPressed(QChar key);

private slots:
    void setupFilter(void);
    void processSelection(const QItemSelection &selected);
    };

#endif /* MAINWINDOW_H_ */
