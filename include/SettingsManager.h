/*
 * SettingsManager.h
 *
 *  Created on: 20.03.2012
 *      Author: tolyan
 */

#ifndef SETTINGSMANAGER_H_
#define SETTINGSMANAGER_H_

#include <QSettings>

#define SETTINGS_FILENAME	"Settings.ini"


class QString;

class SettingsManager: public QSettings
    {
Q_OBJECT

public:
    SettingsManager(QObject* parent = NULL);
    virtual ~SettingsManager();

    signed char openNewSettingsFile(QString& newFileName);
private:
    static QString SettingsFileName();
    static QString getHomePath();
    };

extern SettingsManager* settings;

#endif /* SETTINGSMANAGER_H_ */
