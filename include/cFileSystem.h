/*
 * cFileSystem.h
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#ifndef CFILESYSTEM_H_
#define CFILESYSTEM_H_

#include <QFileSystemModel>
#include <QStringList>

class QVariant;
class QModelIndex;
class OperationMap;

class cFileSystem: public QFileSystemModel
    {
Q_OBJECT
public:
    cFileSystem(OperationMap* opMap, QObject *parent = NULL);
    ~cFileSystem();

    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
	    int role) const;

private:
    OperationMap* opMap;
    QStringList visableOPcodes;
    };

#endif /* CFILESYSTEM_H_ */
