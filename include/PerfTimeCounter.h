/*
 * PerfTimeCounter.h
 *
 *  Created on: 30.05.2012
 *      Author: tolyan
 */

#ifndef PERFTIMECOUNTER_H_
#define PERFTIMECOUNTER_H_

class PerfTimeCounter
    {
public:
    PerfTimeCounter();
    virtual ~PerfTimeCounter();

    void Start(void);
    unsigned long Catch();

private:
    unsigned long GetTickCount(void);

    unsigned long starttime;
    };

#endif /* PERFTIMECOUNTER_H_ */
