/*
 * HotKeyFilter.h
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#ifndef HOTKEYFILTER_H_
#define HOTKEYFILTER_H_

#include <QObject>
#include <QSet>
#include <QChar>

class QEvent;

class HotKeyFilter: public QObject
    {
Q_OBJECT
public:
    HotKeyFilter(QObject* parent = NULL);
    ~HotKeyFilter();

public slots:
    void AddKey(QChar key);

protected:
    virtual bool eventFilter(QObject* obj, QEvent* event);

private:
    QSet<QChar> registredHotkeys;

signals:
    void validKey(QChar);
    };

#endif /* HOTKEYFILTER_H_ */
