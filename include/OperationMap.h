/*
 * OperationMap.h
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#ifndef OPERATIONMAP_H_
#define OPERATIONMAP_H_

/*
 * Применяется QMap, для поиска по имяни
 */

#include <QMap>
#include <QString>
#include <QStringList>
#include <QObject>

#define __DESTINATION 	"./test"

#define MV_DEFAULT_COMMAND	"mv"
#define RM_DEFAULT_COMMAND	"rm -f"
#define CP_DEFAULT_COMMAND	"cp -f"

class OperationMap: public QObject
    {
Q_OBJECT

public:
    enum enFOpCode
	{
	Op_Code_NOOP = 0,
	Op_Code_REMOVE = 1,
	Op_Code_MOVE_TO = 2,
	Op_Code_CP = 3,
	};

    struct sOperation
	{
	enum enFOpCode opCode;
	QString destinationFile;
	int result;
	};

    OperationMap(QObject *parent = NULL);
    ~OperationMap();

    int getOpStatus(const QString &filename);
    sOperation getInfo(const QString& file);

public slots:
    int executeAll(void);
    int execute(const QString& file);
    bool addItem(const QString &filename, const enFOpCode opcode,
	    const QString &destination = QString());
    bool replaceItem(const QString &filename, const enFOpCode opcode,
	    const QString &destination = QString());

private:
    QMap<QString, sOperation> items;
    QStringList commands;
    QStringList visableOPcodes;

    bool execute(const QMap<QString, sOperation>::iterator it);
    QStringList makeCommandLineArgs(const QString &source,
	    const sOperation &op);

signals:
    void updated();
    void progressUpdate(int); // тут проценты
    };

#endif /* OPERATIONMAP_H_ */
