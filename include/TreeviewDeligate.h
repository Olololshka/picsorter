/*
 * TreeviewDeligate.h
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#ifndef TREEVIEWDELIGATE_H_
#define TREEVIEWDELIGATE_H_

#include <QItemDelegate>

class QPainter;
class QStyleOptionViewItem;
class QModelIndex;

class TreeviewDeligate: public QItemDelegate
    {
Q_OBJECT
public:
    TreeviewDeligate(QObject *parent = NULL);
    ~TreeviewDeligate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
	    const QModelIndex &index) const;
    };

#endif /* TREEVIEWDELIGATE_H_ */
