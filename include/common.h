/*
 * Comon.h
 *
 *  Created on: 02.02.2012
 *      Author: shiloxyz
 */

#ifndef COMON_H_
#define COMON_H_

#include <assert.h>

#ifndef NULL
#define NULL	(void*)0
#endif

// макросы упарвления флагами
#define _SET_FLAG(Flags, Flag)   	(Flags |= (Flag))
#define _UNSET_FLAG(Flags, Flag) 	(Flags &= ~(Flag))
#define _TOUGLE_FLAG(Flags, Flag) 	(Flags ^= (Flag))

#define _CHECK_FLAG(Flags, Flag, State) ((Flags & Flag) ? (State) : (!State))

#define __DELETE(x)			{\
					assert(x != NULL);\
					delete  x;\
					x = NULL;\
					}

#endif /* COMON_H_ */
