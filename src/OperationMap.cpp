/*
 * OperationMap.cpp
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#include <QMapIterator>
#include <QVariant>
#include <QProcess>
#include <QApplication>

#include "Log.h"
#include "SettingsManager.h"

#include "OperationMap.h"

OperationMap::OperationMap(QObject *parent) :
	QObject(parent)
    {
    commands
	    << ""
	    << settings->value(trUtf8("Commands/rm"), RM_DEFAULT_COMMAND).toString()
	    << settings->value(trUtf8("Commands/mv"), MV_DEFAULT_COMMAND).toString()
	    << settings->value(trUtf8("Commands/cp"), CP_DEFAULT_COMMAND).toString();
    }

OperationMap::~OperationMap()
    {
    }

bool OperationMap::addItem(const QString &filename, const enFOpCode opcode,
	const QString &destination)
    {
    if (!items.contains(filename))
	{
	sOperation op =
	    {
	    opcode, QString(destination), -1
	    };
	items[filename] = op;
	LOG_DEBUG(
		trUtf8("\"%1\" added to action list (dest=\"%2\"), op=%3").arg(filename).arg(op.destinationFile).arg(opcode));
	emit
	updated();
	return true;
	}
    else
	{
	LOG_WARNING(trUtf8("\"%1\" already in list!").arg(filename));
	return false;
	}
    }

bool OperationMap::replaceItem(const QString &filename, const enFOpCode opcode,
	const QString &destination)
    {
    if (items.contains(filename))
	{

	items.remove(filename);
	sOperation op =
	    {
	    opcode, QString(destination), -1
	    };
	items[filename] = op;
	LOG_DEBUG(
		trUtf8("\"%1\" replaced (dest=\"%2\"), op=%3").arg(filename).arg(op.destinationFile).arg(opcode));
	emit
	updated();
	return true;
	}
    else
	{
	LOG_WARNING(
		trUtf8("No files named \"%1\" in action list, adding...").arg(filename));
	return addItem(filename, opcode, destination);
	}
    }

int OperationMap::getOpStatus(const QString & filename)
    {
    return items[filename].result;
    }

int OperationMap::executeAll(void)
    {
    int sucess = 0;
    int processed = 0;
    qreal all = items.count();
    QMap<QString, sOperation>::iterator it = items.begin();
    for (; it != items.end(); ++it)
	{
	sucess += (int) execute(it);
	emit
	progressUpdate((int) (++processed * 100 / all));
	qApp->processEvents();
	}
    items.clear();
    return sucess;
    }

int OperationMap::execute(const QString& file)
    {
    QMap<QString, sOperation>::iterator it = items.find(file);
    return (int) execute(it);
    }

bool OperationMap::execute(const QMap<QString, sOperation>::iterator it)
    {
    QStringList args = makeCommandLineArgs(it.key(), it.value());
    if (args.isEmpty())
	{
	LOG_ERROR(trUtf8("Empty command created"));
	return false;
	}
    QProcess process;
    process.start(commands[it.value().opCode], args);
    process.waitForFinished();
    if ((process.state() != QProcess::NotRunning) || (process.exitCode() != 0))
	{
	LOG_ERROR(
		trUtf8("Command \"%1\" unsuccessful").arg(commands[it.value().opCode] + ' ' + args.join(" ")));
	it.value().result = 1;
	return false;
	}
    LOG_DEBUG(
	    trUtf8("Command \"%1\" successful").arg(commands[it.value().opCode] + ' ' + args.join(" ")));
    it.value().result = 0;
    return true;
    }

QStringList OperationMap::makeCommandLineArgs(const QString & source,
	const sOperation &op)
    {
    QStringList args;
    if (op.opCode != Op_Code_NOOP)
	{
	args << source << op.destinationFile;
	if (op.opCode != Op_Code_REMOVE)
	    args << op.destinationFile;
/*
	args << "\'" + source + "\'";
	if (op.opCode != Op_Code_REMOVE)
	    args << op.destinationFile;
*/
	}
    return args;
    }

OperationMap::sOperation OperationMap::getInfo(const QString& file)
    {
    if (items.contains(file))
	return items[file];
    else
	return sOperation();
    }
