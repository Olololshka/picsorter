//-----------------------------------------------------------------
#include <QApplication>

#include "Revision.h"
#include "common.h"
#include "SettingsManager.h"
#include "Log.h"
#include "MainWindow.h"

#include "main.h"
//-----------------------------------------------------------------

int main(int argc, char *argv[])
    {
    QApplication App(argc, argv);
    App.setApplicationName("PicSorter");
    App.setApplicationVersion(GetRevisionNumber());

    settings = new SettingsManager;
    LOG = new Log;

    MainWindow mainwin;
    settings->setParent(&mainwin);
    LOG->setParent(&mainwin);

    mainwin.show();
	
    int res = App.exec();

    return res;
    }
//-----------------------------------------------------------------
