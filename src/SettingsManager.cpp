/*
 * SettingsManager.cpp
 *
 *  Created on: 20.03.2012
 *      Author: tolyan
 */

#include <QString>
#include <QSettings>
#include <QProcess>
#include <QDebug>
#include <QApplication>
#include <QStringList>

#include "SettingsManager.h"

SettingsManager* settings;

SettingsManager::SettingsManager(QObject* parent) :
	QSettings(getHomePath() + trUtf8("/.PicSorter/") + SettingsFileName(),
		QSettings::IniFormat, parent)
    {
    qDebug() << getHomePath() + trUtf8("/.PicSorter/") + SettingsFileName();
    setIniCodec("UTF-8");
    }

SettingsManager::~SettingsManager()
    {
    sync();
    }

signed char SettingsManager::openNewSettingsFile(QString& newFileName)
    {
    //TODO
    return 0;
    }

QString SettingsManager::SettingsFileName()
    {
    QStringList args = qApp->arguments();
    QList<QString>::Iterator it = args.begin();
    for (; it < args.end(); ++it)
	{
	if (*it == trUtf8("-f"))
	    {
	    if (++it < args.end())
		return *it;
	    }
	}
    return trUtf8(SETTINGS_FILENAME);
    }

QString SettingsManager::getHomePath()
    {
    QProcess process;
    QStringList env = QProcess::systemEnvironment();
    QStringList::iterator it = env.begin();
    for (; it != env.end(); ++it)
	{
	QString t = *it;
	t.resize(5);
	if (t == "HOME=")
	    {
	    it->remove(0, it->indexOf('=') + 1);
	    return *it;
	    }
	}
    return QString();
    }
