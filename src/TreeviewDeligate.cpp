/*
 * TreeviewDeligate.cpp
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#include <QPainter>

#include "TreeviewDeligate.h"

TreeviewDeligate::TreeviewDeligate(QObject *parent) :
	QItemDelegate(parent)
    {
    }

TreeviewDeligate::~TreeviewDeligate()
    {
    }

void TreeviewDeligate::paint(QPainter *painter,
	const QStyleOptionViewItem & option, const QModelIndex & index) const
    {
    QRect myColumnRect = option.rect;
    if ((!(option.state & QStyle::State_Selected))
	    && ((index.column() == 4) || (index.column() == 5)))
	{
	Qt::GlobalColor color =
		(index.column() == 4) ? (Qt::yellow) : (Qt::green);
	painter->setBrush(QBrush(color));
	painter->setPen(QPen(color));
	painter->drawRect(myColumnRect);
	}
    if ((!(option.state & QStyle::State_Selected)) && (index.column() == 6))
	{
	Qt::GlobalColor color;
	switch (index.data(Qt::DisplayRole).toInt())
	    {
	case -1:
	    color = Qt::yellow;
	    break;
	case 0:
	    color = Qt::green;
	    break;
	default:
	    color = Qt::red;
	    break;
	    }
	painter->setBrush(QBrush(color));
	painter->setPen(QPen(color));
	painter->drawRect(myColumnRect);
	}
    QItemDelegate::paint(painter, option, index);
    }
