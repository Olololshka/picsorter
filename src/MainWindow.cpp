/*
 * MainWindow.cpp
 *
 *  Created on: 22.08.2012
 *      Author: tolyan
 */

#include <QSplitter>
#include <QTreeView>
#include <QLabel>
#include <QApplication>
#include <QDir>
#include <QLineEdit>
#include <QString>
#include <QSizePolicy>
#include <QVBoxLayout>
#include <QItemSelection>
#include <QDebug>
#include <QFile>
#include <QPixmap>
#include <QHeaderView>
#include <QPushButton>
#include <QProgressBar>

#include "Log.h"
#include "PerfTimeCounter.h"
#include "cFileSystem.h"
#include "TreeviewDeligate.h"
#include "HotKeyFilter.h"
#include "OperationMap.h"

#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags) :
	QMainWindow(parent, flags)
    {
    opMap = new OperationMap(this);

    filesystem = new cFileSystem(opMap, this);
    filesystem->setRootPath(QApplication::applicationDirPath());
    QModelIndex root;
    QDir dir;
    dir.setPath(QApplication::arguments().last());
    if (dir.exists()) //является ли последний ргумент командв существующей папкой?
	{
	root = filesystem->index(QApplication::arguments().last());
	}
    else
	{
	root = filesystem->index(QDir::current().path()); //верент "/", в Шиндошс c:/
	}
    selectionModel = new QItemSelectionModel(filesystem);

    filesList = new QTreeView(this);
    filesList->setModel(filesystem);
    for (int i = 1; i < filesystem->columnCount() - 3; ++i)
	filesList->hideColumn(i);
    filesList->setRootIndex(root);
    filesList->setSelectionModel(selectionModel);
    filesList->setItemDelegate(new TreeviewDeligate(filesList)); // назначение делигата, перент - таблица
    //режим растягивания колонок
    filesList->header()->setStretchLastSection(false);
    filesList->header()->setResizeMode(0, QHeaderView::Stretch);
    filesList->header()->setResizeMode(filesystem->columnCount() - 3,
	    QHeaderView::ResizeToContents);
    filesList->header()->setResizeMode(filesystem->columnCount() - 2,
	    QHeaderView::ResizeToContents);
    filesList->header()->setResizeMode(filesystem->columnCount() - 1,
	    QHeaderView::ResizeToContents);

    startButton = new QPushButton(trUtf8("Начать!"));
    progressBar = new QProgressBar(this);
    progressBar->setRange(0, 100);
    progressBar->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    pic = new QLabel(this);
    pic->setAlignment(Qt::AlignCenter); //выровняет картинку по центру лабела
    pic->setMinimumSize(640, 480);

    filterEdit = new QLineEdit(this);

    QWidget *leftArea = new QWidget(this);
    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addWidget(filesList);
    leftLayout->addWidget(filterEdit);
    leftLayout->addWidget(startButton);
    leftLayout->addWidget(progressBar);
    leftArea->setLayout(leftLayout);

    QSplitter *vertSpliter = new QSplitter(Qt::Horizontal, this);
    vertSpliter->addWidget(pic);
    vertSpliter->addWidget(leftArea);
    setCentralWidget(vertSpliter);

    hotkeyFilter = new HotKeyFilter(filesList);
    filesList->installEventFilter(hotkeyFilter); //ставим фильтр хоткеев на дерево

    connect(filterEdit, SIGNAL(textChanged(const QString&)), this,
	    SLOT(setupFilter(void)));
    connect(
	    selectionModel,
	    SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
	    this, SLOT(processSelection(const QItemSelection &)));
    connect(hotkeyFilter, SIGNAL(validKey(QChar)), this,
	    SLOT(hotKeyPressed(QChar)));
    connect(startButton, SIGNAL(clicked()), opMap, SLOT(executeAll(void)));
    connect(opMap, SIGNAL(progressUpdate(int)), progressBar,
	    SLOT(setValue(int)));
    //connect(startButton, SIGNAL(clicked()), progressBar, SLOT(reset()));

    ReadSettings();
    }

void MainWindow::setupFilter(void)
    {
    filesystem->setNameFilters(filterEdit->text().split(" "));
    }

void MainWindow::processSelection(const QItemSelection & selected)
    {
    /*
     * QItemSelection - это QList со списком выделеных
     */
    if (selected.isEmpty() || fSelectionChanging)
	return;
    QItemSelectionRange item = selected.at(0); // выделяем одну строку из всех выделеных (первую)
    /*
     QMap<int, QVariant> data = filesystem->itemData(item.indexes().at(0)); // читаем содерживое выделеной строки
     QString filename = data.take(0).toString(); //читаем первое поле = имя, к сожалению оно не содержит абсолютного пути
     qDebug() << filename.toAscii();
     */
    QModelIndex SelectedPos = item.indexes().at(0);
    QString fullName = filesystem->fileInfo(SelectedPos).filePath(); //ок работает!

    if (!filesystem->fileInfo(SelectedPos).isDir()) // это не папка?
	{
	LOG_DEBUG(trUtf8("Selected: %1").arg(fullName));

	// загружаем картинку
	QPixmap pixmap(fullName);
	if (!pixmap.isNull())
	    {
	    PerfTimeCounter perf;
	    perf.Start();
	    pic->setPixmap(
		    pixmap.scaled(pic->size(), Qt::KeepAspectRatio,
			    Qt::SmoothTransformation));
	    LOG_INFO(trUtf8("...Loaded for %1 [ms]").arg(perf.Catch()));
	    }
	else
	    {
	    LOG_ERROR(trUtf8("Error loading %1 as image").arg(fullName));
	    }
	}
    else
	{
	LOG_DEBUG(trUtf8("\"%1\" is a directory").arg(fullName));
	}
    }

void MainWindow::hotKeyPressed(QChar keycode)
    {
    OperationMap::sOperation op = opTemplate[keycode];
    LOG_DEBUG(
	    trUtf8("Key: %1, op: %2, dest: %3").arg(keycode).arg(op.opCode).arg(op.destinationFile));
    QModelIndex SelectedPos = selectionModel->selectedRows().at(0);
    if (!filesystem->fileInfo(SelectedPos).isDir())
	{
	if (!opMap->addItem(filesystem->fileInfo(SelectedPos).filePath(),
		op.opCode, op.destinationFile))
	    opMap->replaceItem(filesystem->fileInfo(SelectedPos).filePath(),
		    op.opCode, op.destinationFile);
	}
    QModelIndex nextPos;
    fSelectionChanging = true;
    for (int i = 0; i < filesystem->columnCount(); ++i)
	{
	nextPos = filesystem->index(SelectedPos.row() + 1, SelectedPos.column(),
		SelectedPos.parent());
	if (!nextPos.isValid())
	    {
	    fSelectionChanging = false;
	    break;
	    }
	selectionModel->select(SelectedPos, QItemSelectionModel::Deselect);
	if (i == filesystem->columnCount() - 1)
	    fSelectionChanging = false;
	selectionModel->select(nextPos, QItemSelectionModel::Select);
	SelectedPos = filesystem->index(SelectedPos.row(), i + 1,
		SelectedPos.parent());
	}
    }

void MainWindow::ReadSettings(void)
    {
    settings->beginGroup(trUtf8("Actions"));
    QStringList allActions = settings->allKeys(); //читаем все действия
    settings->endGroup();
    QStringList::iterator it = allActions.begin();
    for (; it != allActions.end(); ++it)
	{
	if (it->contains('/')) //отрезаем все, кроме названия действия
	    {
	    int slashPos = it->indexOf('/');
	    it->remove(slashPos, it->length() - slashPos);
	    }
	else
	    it->clear(); // если там не было "/" - в топку
	}
    allActions.removeDuplicates(); // удаяем дупликаты, теперь список содержит возможные действия
    foreach(QString key, allActions)
	{
	QChar ch =
		settings->value(trUtf8("/Actions/%1/Key").arg(key), 0).toString().at(
			0);
	if (ch == '\0')
	    LOG_WARNING(
		    trUtf8("Key \"%1\" invalid, ignoring action \"%2\"").arg(trUtf8("Actions/%1/Key").arg(key)).arg(key));
	else
	    {
	    QString OpCodeStr =
		    settings->value(trUtf8("/Actions/%1/Action").arg(key),
			    QString()).toString();
	    if (OpCodeStr == trUtf8("MV"))
		{
		QString Foldername = createFolder(key);
		if (!Foldername.isEmpty())
		    {
		    opTemplate[ch] = OperationMap::sOperation
			{
			OperationMap::Op_Code_MOVE_TO, Foldername, 0
			};
		    hotkeyFilter->AddKey(ch);
		    }
		continue;
		}
	    if (OpCodeStr == trUtf8("CP"))
		{
		QString Foldername = createFolder(key);
		if (!Foldername.isEmpty())
		    {
		    opTemplate[ch] = OperationMap::sOperation
			{
			OperationMap::Op_Code_CP, Foldername, 0
			};
		    hotkeyFilter->AddKey(ch);
		    }
		continue;
		}
	    else if (OpCodeStr == trUtf8("RM"))
		{
		opTemplate[ch] = OperationMap::sOperation
		    {
		    OperationMap::Op_Code_REMOVE, QString(), 0
		    };
		hotkeyFilter->AddKey(ch);
		continue;
		}
	    LOG_WARNING(
		    trUtf8("Key \"%1\" invalid, ignoring action \"%2\"").arg(trUtf8("Actions/%1/Action").arg(key)).arg(key));
	    }
	}
    LOG_INFO(trUtf8("List of available Actions: ") + allActions.join(", "));
    }

QString MainWindow::createFolder(const QString& ActionName)
    {
    QString destinationFolder =
	    settings->value(trUtf8("/Actions/%1/Destination").arg(ActionName),
		    QString()).toString();
    if (destinationFolder.isEmpty())
	destinationFolder = ActionName;
    QDir dir;
    if (!dir.exists(destinationFolder)) // папка существует?
	{
	if (!dir.mkdir(destinationFolder)) // создалась?
	    {
	    //fail папка не создалась
	    LOG_ERROR(
		    trUtf8("Cann't create folder \"%1\" for action \"%2\"").arg(destinationFolder).arg(ActionName));
	    return QString();
	    }
	else
	    LOG_INFO(
		    trUtf8("Creating Folder \"%1\" for action \"%2\"").arg(destinationFolder).arg(ActionName));
	}
    return destinationFolder;
    }

