/*
 * HotKeyFilter.cpp
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */

#include <QEvent>
#include <QKeyEvent>

#include "Log.h"

#include "HotKeyFilter.h"

HotKeyFilter::HotKeyFilter(QObject* parent) :
	QObject(parent)
    {
    }

HotKeyFilter::~HotKeyFilter()
    {
    }

bool HotKeyFilter::eventFilter(QObject *obj, QEvent *event)
    {
    QEvent::Type type = event->type();
    if (type == QEvent::KeyPress)
	{
	QKeyEvent *keyev = static_cast<QKeyEvent*>(event);
	if (keyev->text().isEmpty())
	    return false;
	QChar key = keyev->text().at(0);
	if (registredHotkeys.contains(key.toUpper()))
	    {
	    LOG_DEBUG(trUtf8("Valid Key: %1").arg(key));
	    emit
	    validKey(key);
	    return true;
	    }
	}
    return true; //фильтр не сработал
    }

void HotKeyFilter::AddKey(QChar key)
    {
    registredHotkeys.insert(key.toUpper());
    }
