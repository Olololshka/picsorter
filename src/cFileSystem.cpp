/*
 * cFileSystem.cpp
 *
 *  Created on: 23.08.2012
 *      Author: tolyan
 */
#include <QVariant>
#include <QString>
#include <QDir>

#include "Log.h"
#include "OperationMap.h"

#include "cFileSystem.h"

cFileSystem::cFileSystem(OperationMap* opMap, QObject *parent) :
	QFileSystemModel(parent)
    {
    this->opMap = opMap;
    visableOPcodes << "" << "RM" << "MV" << "CP";
    }

cFileSystem::~cFileSystem()
    {
    }

int cFileSystem::columnCount(const QModelIndex & parent) const
    {
    return (parent.column() > 0) ? (0) : (7); // имя, размер, тип, дата + 3*(моя_колонка)
    }

Qt::ItemFlags cFileSystem::flags(const QModelIndex & index) const
    {
    if (!index.isValid())
	return Qt::NoItemFlags;
    if ((index.column() == 4) || (index.column() == 5) || (index.column() == 6)) //мои колонки
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    return QFileSystemModel::flags(index);
    }

QVariant cFileSystem::data(const QModelIndex & index, int role) const
    {
    if (!index.isValid())
	return QVariant();
    /*
     if (role == Qt::DisplayRole && index.column() == 4)
     return QString::number(-1); // здесь нужно вернуть QVariant заполнения для моей колонки
     */
    if (role == Qt::DisplayRole && (index.column() > 3 && index.column() < 7))
	{
	QString Filename = this->fileInfo(index).filePath(); //имя
	OperationMap::sOperation op = opMap->getInfo(Filename); // состояние
	bool opValid = (op.opCode != OperationMap::Op_Code_NOOP);
	if (role == Qt::DisplayRole && index.column() == 4)
	    return opValid ?
		    (visableOPcodes[opMap->getInfo(Filename).opCode]) :
		    QVariant(); // код операции
	if (role == Qt::DisplayRole && index.column() == 5)
	    return opValid ?
		    (opMap->getInfo(Filename).destinationFile) : QVariant();
	if (role == Qt::DisplayRole && index.column() == 6)
	    return opValid ? (opMap->getInfo(Filename).result) : QVariant();
	}
    return QFileSystemModel::data(index, role);
    }

QVariant cFileSystem::headerData(int section, Qt::Orientation orientation,
	int role) const
    {
    if (section == 5 && orientation == Qt::Horizontal
	    && role == Qt::DisplayRole)
	return tr("Operation"); // это заголовок
    if (section == 5 && orientation == Qt::Horizontal
	    && role == Qt::DisplayRole)
	return tr("Destination");
    if (section == 6 && orientation == Qt::Horizontal
	    && role == Qt::DisplayRole)
	return tr("Res");
    return QFileSystemModel::headerData(section, orientation, role);
    }
