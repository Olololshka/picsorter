/*
 * Log.cpp
 *
 *  Created on: 29.06.2012
 *      Author: tolyan
 */

#include <QMutex>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QLabel>
#include <QDebug>

#include "common.h"

#include "Log.h"

#define __SAFE	(1)

Log* LOG;

Log::Log(QObject* parent) :
	QObject(parent)
    {
    Logfile = NULL;
    textstream = NULL;
    pInfoString = NULL;
    OperationInProgressMutex = new QMutex;
    setLogFileName(trUtf8(LOG_DEFAULT_FILENAME));
    setLogLevel();
    setLogDateTimeFormat();
    addMessage(trUtf8("Log started."), LOG_INFO);
    }

Log::~Log()
    {
    setInfoString((QLabel*) NULL);
#if (__SAFE == 0)
    /*
     * В небезопасном режим придется удалять Logfile вручную, зато можно выслать это сообщение
     * В безопасном и правильном режиме перент Logfile.patent() == this, поэтому
     * ~Logfile перед входом сюда, и его уже нельзя юзать!
     */
    addMessage(trUtf8("~Log called."), LOG_DEBUG);
    Logfile->close();
    __DELETE(Logfile);
#endif
    __DELETE(textstream);
    __DELETE(OperationInProgressMutex);
    }

void Log::addMessage(const QString & text, enLogLevel Level,
	const char* additionalText)
    {
    if (Level <= getLogLevel())
	{ //игнорируем, ибо больше уровня логирования
	OperationInProgressMutex->lock();
	if (pInfoString)
	    pInfoString->setText(text); // FIXME упало в сегфолт
	QString result;
	QDateTime currentdateTime = QDateTime::currentDateTime();
	switch (Level)
	    {
	case LOG_DEBUG:
	    result.append(trUtf8("[DEBUG]"));
	    break;
	case LOG_INFO:
	    result.append(trUtf8("[INFO]"));
	    break;
	case LOG_WARNING:
	    result.append(trUtf8("[WARNING]"));
	    break;
	case LOG_ERROR:
	    result.append(trUtf8("[ERROR]"));
	    break;
	case LOG_FATAL:
	    result.append(trUtf8("[FATAL]"));
	    break;
	default:
	    result.append(trUtf8("[DEBUG]"));
	    break;
	    }
	if (additionalText != NULL)
	    result.append(trUtf8("[%1]\t").arg(additionalText));
	else
	    result.append('\t');
	result.append(text);
	qDebug() << result; // FIXME упало на new() SIGSEGV: segfault

	result.push_front(
		trUtf8("[%1][%2]").arg(
			currentdateTime.date().toString(LogDateFormat)).arg(
			currentdateTime.time().toString(LogTimeFormat)));
	(*textstream) << result << endl;
	OperationInProgressMutex->unlock();
	}
    }

void Log::setLogFileName(const QString & name)
    {
    OperationInProgressMutex->lock();
    if (textstream != NULL)
	__DELETE(textstream);
    if (Logfile != NULL)
	{
	Logfile->close();
	__DELETE(Logfile);
	}
    QString _name = name;
    _name = _name.arg(QDateTime::currentDateTime().toString(Qt::ISODate));
    _name.replace(QChar(':'), QChar('_'), Qt::CaseInsensitive);
#if (__SAFE == 1)
    Logfile = new QFile(_name, this);
#else
    Logfile = new QFile(_name);
#endif
    Logfile->open(QIODevice::WriteOnly | QIODevice::Text);
    textstream = new QTextStream(Logfile);
    OperationInProgressMutex->unlock();
    }

void Log::setLogLevel(enLogLevel logLevel)
    {
    OperationInProgressMutex->lock();
    CurrentLogLevel = logLevel;
    OperationInProgressMutex->unlock();
    }

Log::enLogLevel Log::getLogLevel(void) const
    {
    Log::enLogLevel res;
    OperationInProgressMutex->lock();
    res = CurrentLogLevel;
    OperationInProgressMutex->unlock();
    return res;
    }

void Log::setLogDateTimeFormat(const QString& DateFormat,
	const QString& TimeFormat)
    {
    OperationInProgressMutex->lock();
    LogDateFormat = DateFormat;
    LogTimeFormat = TimeFormat;
    OperationInProgressMutex->unlock();
    }

void Log::setInfoString(QLabel *pInfoString)
    {
    OperationInProgressMutex->lock();
    this->pInfoString = pInfoString;
    OperationInProgressMutex->unlock();
    }
